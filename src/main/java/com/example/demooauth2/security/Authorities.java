package com.example.demooauth2.security;

public enum  Authorities {
    ROLE_ANONYMOUS,
    ROLE_USER,
    ROLE_ADMIN
}
